import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
class Transport {
    private end: number;
    start: number;
    constructor(end = 20, start = 0) {
        this.end = end;
        this.start = start;
    }
    getData = async () => {
        try {
            const response = await fetch(`https://pokeapi.co/api/v2/pokemon?limit=${this.end}&offset=${this.start}.`);
            if (!response.ok) {
                throw new Error('Failed to fetch data');
            }

            const data = await response.json();
            return data.results;
        } catch (error) {
            console.error(error);
            return [];
        }
    }

    async makeRequest(url: string, options: AxiosRequestConfig): Promise<any> {
        try {
            const response: AxiosResponse = await axios.request({
                url,
                ...options
            });

            return response.data;
        } catch (error) {
            console.error('Error making request:', error);
            throw new Error('Failed to make request');
        }
    }

     // Пока неиспользующиеся запросы
    async get(url: string, config?: AxiosRequestConfig): Promise<any> {
        return this.makeRequest(url, { ...config, method: 'get' });
    }

    async post(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
        return this.makeRequest(url, { ...config, method: 'post', data });
    }

    async put(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
        return this.makeRequest(url, { ...config, method: 'put', data });
    }

    async patch(url: string, data?: any, config?: AxiosRequestConfig): Promise<any> {
        return this.makeRequest(url, { ...config, method: 'patch', data });
    }

    async delete(url: string, config?: AxiosRequestConfig): Promise<any> {
        return this.makeRequest(url, { ...config, method: 'delete' });
    }

}
export default new Transport;

